//
//  profileVC.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 28/07/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
import SDWebImage

class ProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
   
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var email: UILabel!
    
    @IBOutlet weak var jobTitle: UILabel!
    
    @IBOutlet weak var phoneNumber: UILabel!
    
    
    var databaseRef : DatabaseReference?
    
    var uuid = NSUUID().uuidString

    override func viewDidLoad() {
        super.viewDidLoad()
        
        databaseRef = Database.database().reference(fromURL: "https://ilivestock-f4b21.firebaseio.com/")
        
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.clipsToBounds = true
        profileImage.contentMode = .scaleAspectFill
        

        profileImage.isUserInteractionEnabled = true
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileVC.choosePhoto))
        profileImage.addGestureRecognizer(recognizer)
        
    
        getUserDataFromServer()
        
        
    }
    

    func choosePhoto() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let updateButton = UIAlertAction(title: "Update Profile Picture", style: UIAlertActionStyle.default, handler: { (action) in
        
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .photoLibrary
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
            
        })
        
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        
        alert.addAction(updateButton)
        alert.addAction(cancelButton)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImageFromPicker : UIImage?
        
        if let editedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            profileImage.image = selectedImage
        }
        
        self.dismiss(animated: true, completion: nil)
        
        self.saveImage()
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    

    func saveImage() {
        
        guard let userID = Auth.auth().currentUser?.uid else {return}
        
        let profileImageFolder = Storage.storage().reference().child("ProfileImage")
        
        if let data = UIImageJPEGRepresentation(profileImage.image!, 0.5) {
            
            
            profileImageFolder.child("\(uuid).jpg").putData(data, metadata: nil, completion: { (metadata, error) in
                
                if error != nil {
                    Alert.showBasic(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    let values = ["Picture": imageUrl]
                    
                    self.updateProfileImage(uid: userID, values: values)
                }
                
            })
            
        }
        
    }
    
    private func updateProfileImage(uid: String, values: [String: Any]) {
        
        let userReference = self.databaseRef?.child("clients").child(uid)
        userReference?.updateChildValues(values, withCompletionBlock: { (error, ref) in
            
            if error != nil {
                Alert.showBasic(title: "Error", message: (error?.localizedDescription)!, viewController: self)
            }
            
            Alert.showBasic(title: "Done", message: "Profile Picture Successfully Updated!", viewController: self)
        })
        
    }
    
    func getUserDataFromServer() {
        
        guard let userID = Auth.auth().currentUser?.uid else {return}
        
        print("seeeee userrrr: \(userID)")
        
        Database.database().reference().child("clients").child(userID).observe(.value, with: { (snapshot) in
            
            
            let value = snapshot.value as? NSDictionary

            self.name.text = value?["Name"] as? String
            self.email.text = value?["Email"] as? String
            self.jobTitle.text = value?["Job Title"] as? String
            self.phoneNumber.text = value?["Phone Number"] as? String

            guard let imageUrl = value?["Picture"] as? String else {return}

            self.profileImage.sd_setImage(with: URL(string: imageUrl) , completed: nil)
            
            
        }, withCancel: nil)
        
    }
   
    @IBAction func logoutButtonClicked(_ sender: Any) {
        
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.synchronize()
        
        do {
            try Auth.auth().signOut()
        } catch {
            print("Unknown error.")
        }
        
        let signIn = self.storyboard?.instantiateViewController(withIdentifier: "signInVC") as! SignInVC
        let delegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController = signIn
        
        delegate.rememberUser()
    }
    
}
