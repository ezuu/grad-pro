//
//  RecordCustomCell.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 21/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit

class RecordCustomCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let tagIdLabel: UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    
    func setupViews() {
        
        
        addSubview(tagIdLabel)
        
        let views = ["tagIdLabel" : tagIdLabel]
        var constraints = [NSLayoutConstraint]()
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[tagIdLabel]-|", options: NSLayoutFormatOptions(), metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[tagIdLabel(50)]", options: NSLayoutFormatOptions(), metrics: nil, views: views)
        
        
        NSLayoutConstraint.activate(constraints)
        
    }
   

}
