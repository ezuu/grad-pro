//
//  registerVC.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 26/07/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import TextFieldEffects
import Firebase
import FirebaseAuth
class RegisterVC: UIViewController {
    
    
    var databaseRef : DatabaseReference?
    
    var name = ""
    var job = ""
    var phoneNumber = ""

    
    @IBOutlet weak var emailText: KaedeTextField!
    
    @IBOutlet weak var passwordText: KaedeTextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        databaseRef = Database.database().reference(fromURL: "https://ilivestock-f4b21.firebaseio.com/")
        // Do any additional setup after loading the view.
        
    }

    
    @IBAction func registerButtonClicked(_ sender: Any) {
        
        guard let email = emailText.text else {return}
        guard let password = passwordText.text else {return}
        
        
        if email != "" && password != "" {
        
            
            Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
                
                if error != nil {
                    Alert.showBasic(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                }
                
                guard let userID = user?.uid else {return}
                
                let value = ["Name": self.name as String,
                              "Email": self.emailText.text! as String,
                              "Job Title": self.job as String,
                              "Phone Number": self.phoneNumber as String,
                              "Picture": "https://firebasestorage.googleapis.com/v0/b/ilivestock-f4b21.appspot.com/o/ProfileImage%2FD4FF23DD-BF8A-4107-A826-5995794F87F8.jpg?alt=media&token=1fa8b6b9-8125-48ba-ba8f-8e117744ba0c"]
                
                self.addUsertoDatabase(userId: userID, values: value)
                
                UserDefaults.standard.set(user?.email, forKey: "user")
                UserDefaults.standard.synchronize()
                
                let delegate : AppDelegate =  UIApplication.shared.delegate as! AppDelegate
                delegate.rememberUser()
        
                
            })
            
        } else {
            
            Alert.showBasic(title: "Error", message: "All fields are required!", viewController: self)
            
        }

    }
    
    private func addUsertoDatabase(userId: String, values: [String: Any]) {
        
        let userReference = self.databaseRef?.child("clients").child(userId)
        
        userReference?.updateChildValues(values, withCompletionBlock: { (error, ref) in
            
            if error != nil {
                Alert.showBasic(title: "Error", message: (error?.localizedDescription)!, viewController: self)
            }
            
        })
        
    }
    

}
