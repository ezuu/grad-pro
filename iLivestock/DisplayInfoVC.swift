//
//  displayInfoVC.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 02/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit

class DisplayInfoVC: UIViewController {
    
    //outlets
    
    @IBOutlet weak var tagID: UILabel!
    
    @IBOutlet weak var breedLabel: UILabel!
    
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var weightLabel: UILabel!
    
    @IBOutlet weak var bornLabel: UILabel!
    
    @IBOutlet weak var temLabel: UILabel!
    
    @IBOutlet weak var noteText: UILabel!
    
    
    
    var tag = ""
    var breed = ""
    var type = ""
    var weight = ""
    var born = ""
    var temp = ""
    var note = ""

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tagID.text = tag
        breedLabel.text = breed
        typeLabel.text = type
        weightLabel.text = weight
        
//        let endOfSentence = born.index(of: " ")!
//        let firstSentence = born[...endOfSentence]
        bornLabel.text = born
        noteText.text = note
        
        let tempDouble = (temp as NSString).doubleValue
        
        if tempDouble >= 38.0 {
            temLabel.backgroundColor = UIColor.red
            temLabel.textColor = UIColor.white
            temLabel.text = "Check up required"
        } else {
            temLabel.text = "Seems to be in good condition"
        }
        
    }


}
