//
//  NotificationVC.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 17/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth



class NotificationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var selectedSegment = 1
    
    var constantTempValue = 38.0
    let cellId = "cellId"
    var temperatureArray = [TemperatureData]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    var recordUnderMonitor = [RecordUnderMonitor]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    var ref : DatabaseReference?
    

    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference(fromURL: "https://ilivestock-f4b21.firebaseio.com/")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelectionDuringEditing = true
        
        tableView.register(TableViewCustomCell.self, forCellReuseIdentifier: cellId)
        
        
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        
//        observeTemperatureValue()
////        observeTemperatureValue()
////        getMonitorRecordData()
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        observeTemperatureValue()
    }
    
    
    
    func observeTemperatureValue() {
        
        guard let userID = Auth.auth().currentUser?.uid else {return}
        
        ref?.child("DynamicData").child(userID).observe(.value, with: {(DataSnapshot) in
            
            if let dynamicDa  = DataSnapshot.value as? NSDictionary {
                
                let livestockTagIDs = dynamicDa.allKeys
                
                self.temperatureArray.removeAll()
                
                for info in livestockTagIDs {
                    
                    let data = dynamicDa[info] as! [String : String]
                    
                    for (_, _) in data {
                        
                        let temp = data["Temperature"]
                        
                        let tempDouble = (temp! as NSString).doubleValue
                        
                        print("Temperature: \(tempDouble)")
                        
                        if tempDouble > self.constantTempValue {
                            let key = data["TagID"]
                            
                            let tempData = TemperatureData(TagID: key!, Temperature: tempDouble)
                            
                            self.temperatureArray.append(tempData)
                        }
                        
                            break
                        }
                    }
                }
            
            
            
        }, withCancel: nil)
        
    }
    
    func getMonitorRecordData() {
        guard let userID = Auth.auth().currentUser?.uid else {return}
        
        
        ref?.child("MonitorRecord").child(userID).observeSingleEvent(of: .value, with: {(DataSnapshot) in
            
            if let monitoredRecord = DataSnapshot.value as? NSDictionary {
                
                let tagIds = monitoredRecord.allKeys
                
                self.recordUnderMonitor.removeAll()
                
                for tagId in tagIds {
                    
                    let records = monitoredRecord[tagId] as! [String : String]
                        
                        let tag = records["TagID"]
                        let temp = records["Temperature"]
                        let diagnosis = records["Diagnosis"]
                        let prognosis = records["Prognosis"]
                        
                        let monitorData = RecordUnderMonitor(Diagnosis: diagnosis!, Prognosis: prognosis!, TagID: tag!, Temperature: temp!)
                        
                        
                        self.recordUnderMonitor.append(monitorData)
        
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
        
        }, withCancel: nil)
        
    }
    
   
    @IBAction func segmentedChanged(_ sender: UISegmentedControl) {
        
        getMonitorRecordData()

    }
    
    //MARK:- TableView Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            print("tempArray: \(temperatureArray.count)")
            return temperatureArray.count
            
        case 1:
            
            print("MonitArr: \(recordUnderMonitor.count)")
            return recordUnderMonitor.count
            
        default:
            break
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TableViewCustomCell
        
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            let sensorData = temperatureArray[indexPath.row]
            print("SensorData: \(sensorData)")
            
            cell.tagIdLabel.text = sensorData.TagID
            cell.temperatureLabel.text = "Temperature: \(sensorData.Temperature)"
            
            break
            
        case 1:
            
            let monitorRec = recordUnderMonitor[indexPath.row]
            print("MonitorRec: \(monitorRec)")
            
            cell.tagIdLabel.text = monitorRec.TagID
            cell.temperatureLabel.text = "Temperature: \(monitorRec.Temperature)"
            
            break
            
        default:
            break
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(100)
    }
    
    
    //MARK:- Add to Monitor Record Feature
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        switch segmentedControl.selectedSegmentIndex {
            
        case 0:
            
            let addToMonitor = UITableViewRowAction(style: .default, title: "Add to Monitor") { (action, indexPath) in
                
                guard let userId = Auth.auth().currentUser?.uid else {return}
            
                let sensorData = self.temperatureArray[indexPath.row]
                
                let tagId = sensorData.TagID
                let temperature = "\(sensorData.Temperature)"
                
                let userReference = self.ref?.child("MonitorRecord").child(userId).child(tagId)
                
                let monitorRecordData = ["TagID": tagId as String,
                                         "Temperature": temperature as String,
                                         "Prognosis": "",
                                         "Diagnosis": ""]
                
                userReference?.updateChildValues(monitorRecordData, withCompletionBlock: { (error, dataRef) in
                    
                    if error != nil {
                        
                        Alert.showBasic(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                    }
                    
                    Alert.showBasic(title: "Done", message: "Successfully added to Monitor Record", viewController: self)
                    
                })
                
            } //completion block
            
            addToMonitor.backgroundColor = #colorLiteral(red: 0.4674592018, green: 0.5209408402, blue: 0.8349696398, alpha: 0.5840682516)
            
            return [addToMonitor]
            
        case 1:
            
            let userId = Auth.auth().currentUser?.uid
            
            let record = self.recordUnderMonitor[indexPath.row]
            
            let recovered = UITableViewRowAction(style: .default, title: "Recovered") { (action, indexPath) in
                
                print("See index:: \(indexPath.row)")
                
                let alertRecover = UIAlertController(title: "Recover", message: "Are you sure the cattle recovered?", preferredStyle: UIAlertControllerStyle.alert)
                let yesButton = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) in
                    
                    Database.database().reference().child("MonitorRecord").child(userId!).child(record.TagID!).removeValue { (error, ref) in
                        
                        if error != nil {
                            print("Failed to delete Record", error!)
                            return
                        }
                        
                        self.recordUnderMonitor.remove(at: indexPath.row)
                        
                    }
                    
                    
                 })
                
                
                let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
                
                alertRecover.addAction(yesButton)
                alertRecover.addAction(cancelButton)
                self.present(alertRecover, animated: true, completion: nil)
                
                
                Alert.showBasic(title: "Recover", message: "Your cattle has been removed from monitoring record!", viewController: self)
                
            } //completion block
            
            recovered.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
            
            return[recovered]
            
        default:
            
            break
   
        }
        
        return []
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         switch segmentedControl.selectedSegmentIndex {
            
         case 0:
            
            break
         case 1:
            
            self.performSegue(withIdentifier: "toDisplayRecord", sender: self)
            
         default:
            
            break
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let indexPath = self.tableView.indexPathForSelectedRow else {return}
        
        let indexNumber = indexPath.row

        let record = self.recordUnderMonitor[indexNumber]
        
        if let displayRecordVc = segue.destination as? DisplayRecordVC {
         
            displayRecordVc.tagId = record.TagID!
            displayRecordVc.temp = record.Temperature
            displayRecordVc.diagnosis = record.Diagnosis!
            displayRecordVc.prognosis = record.Prognosis!
            
            
        }
        
    }
 
 
}






