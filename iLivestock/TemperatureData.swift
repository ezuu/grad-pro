//
//  TemperatureData.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 24/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import Foundation

struct TemperatureData {
    
    var TagID : String
    var Temperature : Double
    
}
