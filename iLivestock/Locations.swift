//
//  Locations.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 06/09/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import MapKit

class Locations: NSObject, MKAnnotation {
    private var coord: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)

    var title : String?
    var coordinate: CLLocationCoordinate2D {
        get {
            return coord
        }
    }
    
//    override init() {
//        self.title = ""
//        self.coordinate = CLLocationCoordinate2D.init()
//    }
//    
    func setCoordinate(newCoordinate: CLLocationCoordinate2D) {
        self.coord = newCoordinate
    }
    
}
