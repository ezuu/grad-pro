//
//  UserNotification.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 25/09/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class UserNotification {
    
    //MARK:- User Notification
    class func basicUserNotification(title: String, body: String, timeInterval: Double, identifier: String) {
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        content.badge = (UIApplication.shared.applicationIconBadgeNumber + 1) as NSNumber
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
}
