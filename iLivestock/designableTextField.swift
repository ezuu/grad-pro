//
//  designableTextField.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 22/07/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit

@IBDesignable

class designableTextField: UITextField {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    //property show up in our attribute inspector
    @IBInspectable var leftImage : UIImage? {
        didSet {
            updateView()
        }
    }
    
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    
    func updateView() {
        
        if let image = leftImage {
            leftViewMode = .always   //if there is image show it
            
            let imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: 20, height: 20))
            
            imageView.image = image
            
            var width = leftPadding + 20
            
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line {
               width = width + 5
            }
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            
            leftView = view
            
        } else {
            //image is nill
            leftViewMode = .never
        }
        
        
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder! : "", attributes: [NSForegroundColorAttributeName : tintColor])
    }

}
