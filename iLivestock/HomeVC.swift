//
//  FirstViewController.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 22/07/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import UserNotifications

class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource,
                UISearchBarDelegate, UISearchResultsUpdating {
   
    @IBOutlet weak var tableView: UITableView!
    
    
    var livestockInfo = [LivestockInfo]()
    var dynamicData = [DynamicData]()
    var vaccinationInfo = [LivestockInfo]()
    
    var filteredArray = [LivestockInfo]()
    var filteredDynamicData = [DynamicData]()
    
    let cellId = "recordCellId"
    var constantTempValue = 38.0

   
    let searchController = UISearchController(searchResultsController: nil)
    
    var isSearching = false
    var key = ""
    var tagNum = ""
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup user notification
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (accepted, error) in
            if !accepted {
                print("access denied")
            }
        }
        
        
        // Setup the Search Controller
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchResultsUpdater = self
       
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        // Setup the TableView
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.allowsMultipleSelectionDuringEditing = true
        
        tableView.register(RecordCustomCell.self, forCellReuseIdentifier: cellId)
   
        observeTemperature()
        handleVaccination()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getDataFromServer()
    }
    
    //MARK:- Retrieve data from LivestockRecord database
    func getDataFromServer() {
        
        guard let userID = Auth.auth().currentUser?.uid else {return}
        Database.database().reference().child("LivestockRecord").child(userID).observe(.value, with: { (FIRDataSnapshot) in
            
            if let values = FIRDataSnapshot.value as? NSDictionary {
                
                let tagIDs = values.allKeys
                
                self.livestockInfo.removeAll()
                
                for cowInfo in tagIDs {
                    let info = LivestockInfo()
                    let tagID = values[cowInfo] as! NSDictionary
                    info.setValuesForKeys(tagID as! [String : Any])
                    self.livestockInfo.append(info)
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
            
        }, withCancel: nil)
        
        
        //extracting dynamic data
        Database.database().reference().child("DynamicData").child(userID).observe(.value, with: {(DataSnapshot) in
            
            if let dynamicDataValues = DataSnapshot.value as? NSDictionary {
                
               // print("Testststts: \(dynamicDataValues)")
                
                let livestockTagIDs = dynamicDataValues.allKeys
                
                for dynamicDataInfo in livestockTagIDs {
                    
                    let data = DynamicData()
                    let temperature = dynamicDataValues[dynamicDataInfo] as! NSDictionary
                    data.setValuesForKeys(temperature as! [String : Any])
                    self.dynamicData.append(data)
                    
                }
                
            }
            
        }, withCancel: nil)
       
    }
    
    //MARK:- Observe Temperature
    func observeTemperature() {
        
        guard let userID = Auth.auth().currentUser?.uid else {return}
    Database.database().reference().child("DynamicData").child(userID).observe(.value, with: {(DataSnapshot) in
            
            if let dynamicDa  = DataSnapshot.value as? NSDictionary {
                
                let livestockTagIDs = dynamicDa.allKeys
                
                for info in livestockTagIDs {
                    
                    let data = dynamicDa[info] as! [String : String]
                    
                    for (_, _) in data {
                        
                        let temp = data["Temperature"]
                        let tempDouble = (temp! as NSString).doubleValue
                        
                        if tempDouble > self.constantTempValue {
                            
                            self.key = data["TagID"]!
                            let body = "Temperature for \(self.key) rised significantly, check your livestock"
                            
                            UserNotification.basicUserNotification(title: "Your livestock temperature increased!", body: body, timeInterval: 3, identifier: "temperature")
                        }
                        break
                    }
                }
            }
            
        }, withCancel: nil)
        
    }
    
    func handleVaccination() {
        
        guard let userID = Auth.auth().currentUser?.uid else {return}
        Database.database().reference().child("LivestockRecord").child(userID).observe(.value, with: { (FIRDataSnapshot) in
            
            if let values = FIRDataSnapshot.value as? NSDictionary {
               let livestocks = values.allKeys
                
                for livestock in livestocks {
                    let vaccinData = values[livestock] as! [String: String]
                    
                    for (_,_) in vaccinData {
                        
                        self.tagNum = vaccinData["TagID"]!
                        let bornDate = vaccinData["Born"]
                        let previousVaccinDate = vaccinData["Vaccination"]
                        
                        self.checkVaccinationDate(prevVaccinationDate: previousVaccinDate, bornDate: bornDate, tagId: self.tagNum)
                        
                        break
                    }
                }
            }
        }, withCancel: nil)
        
    }
    
    func checkVaccinationDate(prevVaccinationDate: String?, bornDate: String?, tagId: String) {
        
        let monthsToAdd = 0
        let daysToAdd = 0
        let yearsToAdd = 0
        
        var dateComponent = DateComponents()
        dateComponent.month = monthsToAdd
        dateComponent.day = daysToAdd
        dateComponent.year = yearsToAdd
        
        let currDate = Date()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        let vaccineD = dateFormatter.date(from: prevVaccinationDate!)
        
        let dueDate = Calendar.current.date(byAdding: dateComponent, to: vaccineD!)
        
        let length = dueDate!.daysBetweenDate(toDate: currDate)
        
        if length == 0 {
            self.tagNum = tagId
        let body = "Vaccination for \(self.tagNum) is due, you're recommended to provide IBR, BVD, PI3, BRSV vaccines."
        UserNotification.basicUserNotification(title: "Your Livestock Vaccionation is Due!", body: body, timeInterval: 15, identifier: "vaccination")
        }
        
    }
    
        // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
 
        if searchController.searchBar.text == "" {
            
            isSearching = false
            
        } else {
            
            isSearching = true
        
            let attributedValue = searchController.searchBar.text
            let tagIdPredicate = NSPredicate(format: "TagID like %@", attributedValue!)
            
            filteredArray = livestockInfo.filter{tagIdPredicate.evaluate(with: $0)}
            
        }
        
        tableView.reloadData()
        
    }
    
    //MARK:- TableView Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching {
            
            return filteredArray.count
            
        }
            
         return livestockInfo.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! RecordCustomCell
        
        if isSearching {

            let filteredInfo = filteredArray[indexPath.row]
            
            cell.tagIdLabel.text = filteredInfo.TagID
            
        } else {
            
            let info = livestockInfo[indexPath.row]
            
            cell.tagIdLabel.text = info.TagID
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(80)
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
   
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        guard let userId = Auth.auth().currentUser?.uid else {
            return
        }
        
        let livestockRecord = livestockInfo[indexPath.row]
        
        let alertDelete = UIAlertController(title: "Delete??", message: "Are you sure you want to Delete?", preferredStyle: UIAlertControllerStyle.alert)
        let yesButton = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { (action) in
            
            Database.database().reference().child("LivestockRecord").child(userId).child(livestockRecord.TagID!).removeValue { (error, ref) in
                
                guard let errorMessage = error?.localizedDescription else {return}
                
                if error != nil {
                    
                    Alert.showBasic(title: "Error", message: errorMessage, viewController: self)
                }
                
                self.livestockInfo.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
                
                Alert.showBasic(title: "Delete successful", message: "You record has been deleted successfully!", viewController: self)
                
            }
            
            })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        
        alertDelete.addAction(yesButton)
        alertDelete.addAction(cancelButton)
        self.present(alertDelete, animated: true, completion: nil)
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            self.performSegue(withIdentifier: "display", sender: self)
    }
    
    
    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let indexPath = self.tableView.indexPathForSelectedRow else {return}
        let indexNumber = indexPath.row
        
        let livestockData = self.livestockInfo[indexNumber]
        let dynamData = self.dynamicData[indexNumber]
        
        if let displayVc = segue.destination as? DisplayInfoVC {
            
            if isSearching {

                let searchedResult = self.filteredArray[indexNumber]
                
                let searchedTagID = searchedResult.TagID
                let predicate = NSPredicate(format: "TagID like %@", searchedTagID!)
                filteredDynamicData = dynamicData.filter{predicate.evaluate(with: $0)}
                let searchedTemp = self.filteredDynamicData[indexNumber]
                
                displayVc.tag = searchedResult.TagID!
                displayVc.born = searchedResult.Born!
                displayVc.note = searchedResult.Note!
                displayVc.weight = searchedResult.Weight!
                displayVc.type = searchedResult.LivestockType!
                displayVc.breed = searchedResult.Breed!
                displayVc.temp = searchedTemp.Temperature!
                
            } else {
                
                displayVc.tag = livestockData.TagID!
                displayVc.born = livestockData.Born!
                displayVc.note = livestockData.Note!
                displayVc.weight = livestockData.Weight!
                displayVc.type = livestockData.LivestockType!
                displayVc.temp = dynamData.Temperature!
                displayVc.breed = livestockData.Breed!
                
            }
            
        }
        
    }
    
}

extension Date {
    func daysBetweenDate(toDate: Date) -> Int {
        let components = Calendar.current.dateComponents([.day], from: self, to: toDate)
        return components.day ?? 0
    }
}
