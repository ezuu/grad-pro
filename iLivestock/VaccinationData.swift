//
//  VaccinationData.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 27/09/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import Foundation
import UIKit
struct VaccinationData {
    var TagID : String
    var Born : String
    var Vaccination : String
    var LivestockType : String
    var Weight : String
}
