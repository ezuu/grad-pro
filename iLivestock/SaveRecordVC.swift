//
//  saveRecordVC.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 30/07/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import TextFieldEffects
import Firebase
import FirebaseDatabase
import FirebaseAuth

class SaveRecordVC: UIViewController {
    
    var ref : DatabaseReference?
    var selectedBreed : String?
    var selectedType : String?
    
    var tagId = ""
    var vaccination = ""
    var weight = ""
    
    
    
    @IBOutlet weak var breedText: KaedeTextField!

    @IBOutlet weak var typeText: KaedeTextField!
    
    @IBOutlet weak var bornText: KaedeTextField!
    
    @IBOutlet weak var noteText: UITextView!
    
    let datePicker = UIDatePicker()
    let breedPicker = UIPickerView()
    let typePicker = UIPickerView()
    
    let breeds = ["Angus",
                  "Australian Lowline",
                  "Barzona",
                  "Belgian Blue",
                  "Braford",
                  "British White",
                  "Jersey",
                  "Maine Anjou",
                  "Norwegian Red",
                  "Normande"]
    
    let type = ["Bull",
                "Cow",
                "Heifer",
                "Steer"]
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference(fromURL: "https://ilivestock-f4b21.firebaseio.com/")
        
        createDatePicker()
        createToolbar()
        toolBar()
        createBreedPicker()
        createTypePicker()
    
        ovserveKeyboardNotifications()
    }
    
    fileprivate func ovserveKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardHide() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
    }
    
    
    func keyboardShow() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        
            self.view.frame = CGRect(x: 0, y: -50, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
    }
    
    func createDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
    
        bornText.inputView = datePicker
        
        datePicker.backgroundColor = UIColor.white
    }
    
    func createToolbar() {
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(SaveRecordVC.donePressed))
        toolbar.setItems([doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        bornText.inputAccessoryView = toolbar
        

    }
    
    func toolBar() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(SaveRecordVC.dismissKeyboard))
        toolbar.setItems([doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        breedText.inputAccessoryView = toolbar
        typeText.inputAccessoryView = toolbar
        
    }
    
    
    
    func dismissKeyboard() {
       view.endEditing(true)
    }
    
    
    func donePressed() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let datePicked = dateFormatter.string(from: datePicker.date)
        
        bornText.text = datePicked
        self.view.endEditing(true)
        
    }
    
    
    func createBreedPicker() {
        
        breedPicker.delegate = self
        
        breedPicker.backgroundColor = UIColor.white
        
        breedText.inputView = breedPicker
    }
    
    func createTypePicker() {
    
        typePicker.delegate = self
        
        typePicker.backgroundColor = UIColor.white
        
        typeText.inputView = typePicker
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        
            if breedText.text != "" && typeText.text != "" && bornText.text != "" {

                guard let userID = Auth.auth().currentUser?.uid else {return}

                //MARK:- Saving Data
                let userReference = self.ref?.child("LivestockRecord").child(userID).child(tagId)

                let recordValues = ["TagID": self.tagId as String,
                                    "Breed": self.breedText.text! as String,
                                    "LivestockType": self.typeText.text! as String,
                                    "Weight": self.weight as String,
                                    "Born": self.bornText.text! as String,
                                    "Note": self.noteText.text as String,
                                    "Vaccination": self.vaccination as String]

                userReference?.updateChildValues(recordValues, withCompletionBlock: { (error, dataRef) in

                    if error != nil {
                        
                        Alert.showBasic(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                    }
                    
                    Alert.showBasic(title: "Done", message: "Record has been saved successfully!", viewController: self)
                })

                clearText()

                self.tabBarController?.selectedIndex = 0
            } else {
                
                Alert.showBasic(title: "Error", message: "Fill all the required fields!", viewController: self)

            }
    }
    
    
    func clearText() {
        self.breedText.text = ""
        self.typeText.text = ""
        self.bornText.text = ""
        self.noteText.text = ""
        self.tagId = ""
        self.vaccination = ""
        self.weight = ""
    }
  
}


extension SaveRecordVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        if pickerView == breedPicker {
            return breeds.count
        } else if pickerView == typePicker {
            return type.count
        }
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == breedPicker {
            return breeds[row]
        } else if pickerView == typePicker {
            return type[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == breedPicker {
           selectedBreed = breeds[row]
           breedText.text = selectedBreed
        } else if pickerView == typePicker {
            selectedType = type[row]
            typeText.text = selectedType
        }
    }
    
}


