//
//  CalendarVC.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 05/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class VaccinationVC: UIViewController, UITableViewDelegate, UITableViewDataSource,
                        UISearchBarDelegate, UISearchResultsUpdating {
    
    @IBOutlet weak var tableView: UITableView!
    
    var ref : DatabaseReference?
    
    let cellId = "cellId"
    var tagNum = ""
    var cowsInfo = [VaccinationData](){
        didSet {
            tableView.reloadData()
        }
    }
    var filteredArray = [VaccinationData]()
    
    let searchController = UISearchController(searchResultsController: nil)
    var isSearching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference(fromURL: "https://ilivestock-f4b21.firebaseio.com/")
        
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchResultsUpdater = self
        
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.allowsMultipleSelectionDuringEditing = true
        tableView.register(RecordCustomCell.self, forCellReuseIdentifier: cellId)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handleLivestockVaccination()
    }
    
    
    
    func handleLivestockVaccination() {
        
        guard let userID = Auth.auth().currentUser?.uid else {return}
    Database.database().reference().child("LivestockRecord").child(userID).observe(.value, with: { (FIRDataSnapshot) in
            
        if let values = FIRDataSnapshot.value as? NSDictionary {
            let livestocks = values.allKeys
            
            let monthsToAdd = 0
            let daysToAdd = 0
            let yearsToAdd = 0
            
            var dateComponent = DateComponents()
            dateComponent.month = monthsToAdd
            dateComponent.day = daysToAdd
            dateComponent.year = yearsToAdd
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            self.cowsInfo.removeAll()
            
            for livestock in livestocks {
                let vaccinData = values[livestock] as! [String: String]
                
                for (_,_) in vaccinData {
                    
                    self.tagNum = vaccinData["TagID"]!
                    let previousVaccinDate = vaccinData["Vaccination"]
                    
                    let vaccineD = dateFormatter.date(from: previousVaccinDate!)
                    
                    let dueDate = Calendar.current.date(byAdding: dateComponent, to: vaccineD!)
                    let currentDate = Date()
                    
                    let length = dueDate!.daysBetweenDate(toDate: currentDate)
                    
                    let born = vaccinData["Born"]
                    let vaccination = vaccinData["Vaccination"]
                    let livesType = vaccinData["LivestockType"]
                    let weight = vaccinData["Weight"]
                    
                    if length == 0 {
                        self.tagNum = vaccinData["TagID"]!
                        let vaccinData = VaccinationData(TagID: self.tagNum, Born: born!, Vaccination: vaccination!, LivestockType: livesType!, Weight: weight!)
                        self.cowsInfo.append(vaccinData)
                    }
                    
                    break
                }
            }
        }
            
            
        }, withCancel: nil)
        
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text == "" {
            
            isSearching = false
            
        } else {
            
            isSearching = true
            
            let attributedValue = searchController.searchBar.text
            filteredArray = cowsInfo.filter({$0.TagID == attributedValue})
            
//            let tagIdPredicate = NSPredicate(format: "TagID like %@", attributedValue!)
//
//            filteredArray = cowsInfo.filter{tagIdPredicate.evaluate(with: $0)}
            
        }
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching {
            return filteredArray.count
        }
        
        return cowsInfo.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! RecordCustomCell
        
        if isSearching {
            let filteredInfo = filteredArray[indexPath.row]
            cell.tagIdLabel.text = filteredInfo.TagID
        } else {
            let info = cowsInfo[indexPath.row]
            cell.tagIdLabel.text = info.TagID
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(80)
    }
    

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let done = UITableViewRowAction(style: .default, title: "Done") { (action, indexPath) in
            
            let monthsToAdd = 0
            let daysToAdd = 0
            let yearsToAdd = 0
            
            var dateComponent = DateComponents()
            dateComponent.month = monthsToAdd
            dateComponent.day = daysToAdd
            dateComponent.year = yearsToAdd
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            guard let userId = Auth.auth().currentUser?.uid else {return}
            
            let vaccinationData = self.cowsInfo[indexPath.row]
            let tagId = vaccinationData.TagID
            let prevVacDate = vaccinationData.Vaccination
            
            let prevVacci = dateFormatter.date(from: prevVacDate)
            
            let dueDate = Calendar.current.date(byAdding: dateComponent, to: prevVacci!)
            let dueDateToString = dateFormatter.string(from: dueDate!)
            
            let userReference = self.ref?.child("LivestockRecord").child(userId).child(tagId)
            let updatedVaccinationDate = ["Vaccination": dueDateToString]
            
            userReference?.updateChildValues(updatedVaccinationDate, withCompletionBlock: { (error, dataRef) in
                if error != nil {
                    Alert.showBasic(title: "Error", message: (error?.localizedDescription)!, viewController: self)
                }
                print("check due: \(dueDateToString)")
            })
        }
        return [done]
    }
    
    
}


extension UIColor {
    convenience init(colorWithHex value: Int, alpha:CGFloat = 1.0) {
        self.init(
            red: CGFloat((value & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((value & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(value & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}

