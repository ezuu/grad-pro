//
//  RecordUnderMonitor.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 27/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import Foundation

struct RecordUnderMonitor {
    
    var Diagnosis : String?
    var Prognosis : String?
    var TagID : String?
    var Temperature : String
    
}
