//
//  TableViewCustomCell.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 21/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit

class TableViewCustomCell: UITableViewCell {

 
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let tagIdLabel: UILabel = {
        
        let label = UILabel()
        //label.text = "Cattle TagID"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    
    let temperatureLabel: UILabel = {
        
        let label = UILabel()
        //label.text = "Temperature: 20"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    
    func setupViews() {
        
        addSubview(tagIdLabel)
        addSubview(temperatureLabel)
        
        let views = ["tagIdLabel" : tagIdLabel, "temperatureLabel" : temperatureLabel]
        var constraints = [NSLayoutConstraint]()
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[tagIdLabel]-|", options: NSLayoutFormatOptions(), metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[tagIdLabel(50)]", options: NSLayoutFormatOptions(), metrics: nil, views: views)
        
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[temperatureLabel(200)]", options: NSLayoutFormatOptions(), metrics: nil, views: views)
        constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-50-[temperatureLabel(50)]", options: NSLayoutFormatOptions(), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    
}
