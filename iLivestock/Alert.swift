//
//  Alert.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 12/09/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import Foundation
import UIKit

class Alert {
    
    //MARK:- Alert 
    class func showBasic(title: String, message: String, viewController: UIViewController) {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            let okButton = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil)
            alert.addAction(okButton)
            viewController.present(alert, animated: true, completion: nil)
    }
    
}
