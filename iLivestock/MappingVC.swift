//
//  SecondViewController.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 22/07/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import CoreLocation

class MappingVC: UIViewController, UISearchBarDelegate, MKMapViewDelegate, CLLocationManagerDelegate {
    
    class CustomPointAnnotation: MKPointAnnotation {
        var imageName: String = ""
    }
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    var ref : DatabaseReference?
    
    let locationManager = CLLocationManager()
    
    var locations = [Locations]()
    
    var searchResults = [Locations]() {
        didSet {
            
            self.mapView.removeAnnotations(self.mapView.annotations)
            
            for location in searchResults {
                
                let coord = location.coordinate
                let point = CustomPointAnnotation()
                
                point.coordinate = CLLocationCoordinate2DMake(coord.latitude, coord.longitude)
                point.title = location.title
                point.imageName = "cowAnnotation.png"
                
                mapView.addAnnotation(point)
            }
        }
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    

    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference(fromURL: "https://ilivestock-f4b21.firebaseio.com/")
        
        mapView.delegate = self
        searchController.searchBar.delegate = self
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            
            locationManager.requestAlwaysAuthorization()
            
        } else if CLLocationManager.authorizationStatus() == .denied {
            
            Alert.showBasic(title: "Please enable location services", message: "Location services were previously denied. Please enable location services for this app in Settings.", viewController: self)
            
        } else if CLLocationManager.authorizationStatus() == .authorizedAlways {
            
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getLivestockGeoLocation()
    }

    func getLivestockGeoLocation() {
        
        guard let userID = Auth.auth().currentUser?.uid else {return}
        
        ref?.child("DynamicData").child(userID).observe(.value, with: { (DataSnapshot) in
            
            if let dynamicDa  = DataSnapshot.value as? NSDictionary {
                
                let livestockTagIDs = dynamicDa.allKeys
                
                self.mapView.removeAnnotations(self.mapView.annotations)
                
                for info in livestockTagIDs {
                    
                    let data = dynamicDa[info] as! [String : String]
                    
                    let annotation = CustomPointAnnotation()
                    
            if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
                for (_, _) in data {
                    
                    let latitude = data["Latitude"]
                    let longitude = data["Longitude"]
                    
                    let latitudeDouble = (latitude! as NSString).doubleValue
                    let longitudeDouble = (longitude! as NSString).doubleValue
                    
                    let coordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitudeDouble, longitude: longitudeDouble)
                    
                    let title = data["TagID"]
                    
                    annotation.title = title
                    annotation.coordinate = coordinate
                    annotation.imageName = "cowAnnotation.png"
                    
                    
                    let span = MKCoordinateSpanMake(0.020, 0.020)
                    
                    let region = MKCoordinateRegionMake(CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude), span)
                    
                    self.mapView.setRegion(region, animated: true)
                    self.mapView.addAnnotation(annotation)
                    
                    break
                }
            } else {
    
                   print("System can't track regions")
                
                }
                
            } //for loop
            
         } //if statement
            
      }, withCancel: nil)
        
    }
    
    
    //Mark:- Customizing Annotation
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if pinView == nil {
            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.canShowCallout = true
            
        } else {
            pinView!.annotation = annotation
        }
        
        if let customPointAnnotation = annotation as? CustomPointAnnotation {
            pinView!.image = UIImage(named: customPointAnnotation.imageName)
        }
        
        return pinView
    }
    
    
    
    @IBAction func searchButton(_ sender: Any) {
        
        searchController.searchBar.becomeFirstResponder()
        searchController.searchBar.barTintColor = #colorLiteral(red: 0.4674592018, green: 0.5209408402, blue: 0.8349696398, alpha: 1)
        searchController.searchBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        present(searchController, animated: true, completion: nil)
        
    }
    
    
    //Mark:- Seach Function
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        let attributedValue = searchBar.text
        let predicate = NSPredicate(format: "title like %@", attributedValue!)
        
        searchResults = locations.filter{predicate.evaluate(with: $0)}
        
    }
    
    
    
    @IBAction func addRegion(_ sender: Any) {
        
        print("Add Region")
        
        guard let longPress = sender as? UILongPressGestureRecognizer else {return}
        
        let touchLocation = longPress.location(in: mapView)
        let coordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)
        let region = CLCircularRegion(center: coordinate, radius: 800, identifier: "geofence")
        
        mapView.removeOverlays(mapView.overlays)
        locationManager.startMonitoring(for: region)
        let circle = MKCircle(center: coordinate, radius: region.radius)
        mapView.add(circle)
        
    }
    
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        guard let circleOverLay = overlay as? MKCircle else {
            return MKOverlayRenderer()
        }
        
        let circleRenderer = MKCircleRenderer(circle: circleOverLay)
        
        circleRenderer.strokeColor = UIColor.red
        circleRenderer.fillColor = UIColor.red
        circleRenderer.alpha = 0.5
        circleRenderer.lineWidth = 1.0
        
        
        return circleRenderer
    }
   
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        mapView.showsUserLocation = true
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        let title = "You entered the region"
        let message = "welcome"
        
        Alert.showBasic(title: title, message: message, viewController: self)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        let title = "You left the region"
        let message = "bye bye"
        
        Alert.showBasic(title: title, message: message, viewController: self)
        
    }
    

}









