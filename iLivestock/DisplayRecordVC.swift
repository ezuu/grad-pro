//
//  DisplayRecordVCViewController.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 28/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class DisplayRecordVC: UIViewController {
    
    
    @IBOutlet weak var tagIdText: UILabel!
    
    @IBOutlet weak var tempText: UILabel!
    
    @IBOutlet weak var diagnosisText: UITextView!
    
    @IBOutlet weak var prognosisText: UITextView!
    
    
    var ref : DatabaseReference?
    
    var tagId = ""
    var temp = ""
    var diagnosis = ""
    var prognosis = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference(fromURL: "https://ilivestock-f4b21.firebaseio.com/")

        
        tagIdText.text = tagId
        tempText.text = temp
        diagnosisText.text = diagnosis
        prognosisText.text = prognosis
        
        ovserveKeyboardNotifications()
        
    }
    
    
    fileprivate func ovserveKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardHide() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
    }
    
    
    func keyboardShow() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.view.frame = CGRect(x: 0, y: -95, width: self.view.frame.width, height: self.view.frame.height)
            
        }, completion: nil)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    
    
    
    @IBAction func saveRecordButtonClicked(_ sender: Any) {
        
        if diagnosisText.text != "" && prognosisText.text != "" {
            
            guard let userId = Auth.auth().currentUser?.uid else {return}
            
            let userReference = self.ref?.child("MonitorRecord").child(userId).child(tagId)
            
            let record = ["Diagnosis": self.diagnosisText.text as String,
                          "Prognosis": self.prognosisText.text as String,
                          "TagID": tagId,
                          "Temperature": temp]
            
            userReference?.updateChildValues(record, withCompletionBlock: { (error, dataRef) in
                
                guard let errorMessage = error?.localizedDescription else {return}
                
                if error != nil {
                    
                    Alert.showBasic(title: "Error", message: errorMessage, viewController: self)
                    
                }
                
                
                Alert.showBasic(title: "Done", message: "Diagnosis and Prognosis has been successfully added to the record!", viewController: self)
                
            })
            
            
        } else {
            
            Alert.showBasic(title: "Error", message: "Please enter diagnisis and prognoisis of cattle!", viewController: self)
        }
        
    } //Button

}
