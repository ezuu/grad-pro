//
//  FirstRegistrationPageVC.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 18/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import TextFieldEffects

class FirstRegistrationPageVC: UIViewController {
    
    @IBOutlet weak var nameText: KaedeTextField!
    
    @IBOutlet weak var jobTitleText: KaedeTextField!
    
    @IBOutlet weak var phoneNumberText: KaedeTextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            
                if segue.identifier == "passToReg" {
                    
                        let registrationVC = segue.destination as! RegisterVC

                        registrationVC.name = nameText.text!
                        registrationVC.job = jobTitleText.text!
                        registrationVC.phoneNumber = phoneNumberText.text!
                    
                   
            }

        }

    @IBAction func nextButtonClicked(_ sender: Any) {
        
        guard let phoneNumber = phoneNumberText.text else {return}
        
        if nameText.text == "" && jobTitleText.text == "" && phoneNumberText.text == "" {
            
            Alert.showBasic(title: "Error", message: "All fields are required!", viewController: self)
            
        } else if !phoneNumber.isValid {
            Alert.showBasic(title: "Invalid Phone Number", message: "Please enter a correct phone number, like 123-456-7890", viewController: self)
        }
        
    }

}


//MARK:- Phone Number Validation
extension String {
    
    var isValid : Bool {
        let phoneRegex = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneNumPredicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneNumPredicate.evaluate(with: self)
    }
    
}

