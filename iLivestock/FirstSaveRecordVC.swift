//
//  FirstSaveRecordVC.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 18/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit
import TextFieldEffects

class FirstSaveRecordVC: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var tagId: KaedeTextField!
    
    @IBOutlet weak var vaccination: KaedeTextField!
    
    @IBOutlet weak var weight: KaedeTextField!
    
    let datePicker = UIDatePicker()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.weight.delegate = self

        createDatePicker()
        createToolbar()
        
    }
    
    
    //MARK:- allowing weight text field to accept only numbers and "."
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        let isNumber = allowedCharacters.isSuperset(of: characterSet)
        
        if isNumber == true {
            return true
        } else {
            if string == "." {
                let countdots = weight.text!.components(separatedBy: ".").count - 1
                
                if countdots == 0 {
                    return true
                } else {
                    if countdots > 0 && string == "." {
                        return false
                    } else {
                        return true
                    }
                }
            } else {
                
                return false
            }
            
        }
    }
    
    func createDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        //datePicker.maximumDate = Date().addingTimeInterval(60 * 60 * 24 * 365)
        
        vaccination.inputView = datePicker
        
        datePicker.backgroundColor = UIColor.white
    }
    
    func createToolbar() {
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePressed))
        toolbar.setItems([doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        vaccination.inputAccessoryView = toolbar
        
        
    }
    
    
    func donePressed() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let datePicked = dateFormatter.string(from: datePicker.date)
        
        vaccination.text = datePicked
        self.view.endEditing(true)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == "passToSave" {
            
            let saveRecordVC = segue.destination as! SaveRecordVC
            
            saveRecordVC.tagId = tagId.text!
            saveRecordVC.vaccination = vaccination.text!
            saveRecordVC.weight = weight.text!
            

        }
        
    }
    
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        
        if tagId.text == "" && vaccination.text == "" && weight.text == "" {
            
            Alert.showBasic(title: "Error", message: "All fields are required!", viewController: self)
        }
    }
    

 
}









