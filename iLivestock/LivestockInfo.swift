//
//  LivestockInfo.swift
//  iLivestock
//
//  Created by Ezaden Seraj on 03/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit

class LivestockInfo: NSObject {

    var TagID : String?
    var Breed : String?
    var LivestockType : String?
    var Weight : String?
    var Born : String?
    var Note : String?
    var Vaccination : String?
    
}
